/**
 * @file circular_list.h
 * Definition of a circular linked list class.
 */
#ifndef _CIRCULAR_LIST_
#define _CIRCULAR_LIST_
#include <boost/iterator/iterator_facade.hpp>
#include <iostream>

namespace atalanta {
    /**
     * circular_list: A circularly linked list.
     */
    template <class T>
    class circular_list {
        private:
            struct list_node {
                list_node * next;
                list_node * prev;
                T data;
                list_node( const T & pdata ) : next( this ), prev( this ), data( pdata ) { }
            };
        public:
            template <class Value>
            class cliterator : public boost::iterator_facade<
                                 cliterator<Value>,
                                 Value,
                                 boost::bidirectional_traversal_tag
                             > {
                public:
                    cliterator() : _node( NULL ), _pos( 0 ) { }
                    explicit cliterator( list_node * node, size_t pos ) : 
                        _node( node ), _pos( pos ) { }
                private:
                    friend class boost::iterator_core_access;
                    friend class circular_list;
                    list_node * _node;
                    size_t _pos;

                    void increment() {
                        if( _node != NULL ) {
                            _node = _node->next;
                            ++_pos;
                        }
                    }

                    void decrement() {
                        if( _node != NULL ) {
                            _node = _node->prev;
                            --_pos;
                        }
                    }

                    bool equal( const cliterator<Value> & other ) const {
                        return _node == other._node && _pos == other._pos;
                    }

                    Value & dereference() const {
                        return _node->data;
                    }
            };

            typedef cliterator<const T> const_iterator;
            typedef cliterator<T> iterator;

            /**
             * Constructs an empty circular list.
             */
            circular_list() : _head( NULL ), _length( 0 ) { }

            /**
             * Destroys a circular list.
             */
            ~circular_list() {
                list_node * curr = _head;
                for( size_t i = 0; i < _length; ++i ) {
                    list_node * tmp = curr->next;
                    delete curr;
                    curr = tmp;
                }
            }

            /**
             * Returns an iterator to the beginning of the circular list.
             */
            iterator begin() {
                return iterator( _head, 0 );
            }

            /**
             * Returns an iterator to the "ending" of the circular list. The
             * "ending" is defined to be the beginning of the list, but after
             * the iterator has been moved forward N times (where N is the
             * number of nodes in the list).
             */
            iterator end() {
                return iterator( _head, _length );
            }

            /**
             * Returns a const_iterator to the beginning of the circular list.
             */
            const_iterator begin() const {
                return const_iterator( _head, 0 );
            }

            /**
             * Returns a const_iterator to the "ending" of the circular list.
             * The "ending" is defined to be the beginning of the list, but
             * after the iterator has been moved forward N times (where N is
             * the number of nodes in the list).
             */
            const_iterator end() const {
                return const_iterator( _head, _length );
            }

            /**
             * Inserts the given data after the node pointed to by the
             * iterator. If the iterator is pointing to nothing, inserts the
             * first node into the list.
             *
             * @param it Iterator to insert after.
             * @param data The data to be inserted.
             */
            void insert( iterator & it, const T & data ) {
                if( it._node == NULL ) {
                    it._node = _head = new list_node( data );
                } else {
                    list_node * node = new list_node( data );
                    node->next = it._node->next;
                    if( node->next != NULL )
                        node->next->prev = node;
                    node->prev = it._node;
                    it._node->next = node;
                }
                ++_length;
            }

            /**
             * Inserts the given data after the ndoe pointed to by the
             * iterator.
             *
             * @param it Iterator to insert after.
             * @param data The data to be inserted.
             */
            void insert_after( iterator & it, const T & data ) {
                insert( it, data );
            }

            /**
             * Inserts the given data before the node pointed to by the
             * iterator. If the iterator is pointing to nothing, inserts the
             * first node into the list.
             */
            void insert_before( iterator & it, const T & data ) {
                iterator oit = it;
                insert( --oit, data );
            }

            /**
             * Removes the node at the given iterator from the list.
             *
             * @param it An iterator to the node to be deleted.
             */
            void remove( iterator & it ) {
                if( _length == 1 ) {
                    _head = NULL;
                } else {
                    it._node->prev->next = it._node->next;
                    it._node->next->prev = it._node->prev;
                    if( it._node == _head )
                        _head = it._node->next;
                }
                delete it._node;
                --_length;
            }

            /**
             * Removes the node pointed to by the iterator.
             *
             * @param it An iterator to the node to be removed.
             */
            void erase( iterator & it ) {
                remove( it );
            }

            /**
             * Sets the head pointer to point at the given iterator.
             *
             * @param it The iterator to the node which should become the head.
             */
            void set_head( iterator & it ) {
                _head = it._node;
            }

            /**
             * Returns the size of the list.
             */
            size_t size() const {
                return _length;
            }

            /**
             * Returns the size of the list.
             */
            size_t length() const {
                return _length;
            }

        private:
            list_node * _head;
            size_t _length;
    };
}
#endif
