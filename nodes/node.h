/**
 * @file node.h
 * Definition of a concrete node class for building distributed systems.
 */
#ifndef _NODE_H_
#define _NODE_H_
#include <stdint.h>

#include <boost/asio.hpp>

#include "nodes/abstract_node.h"

namespace atalanta {
/**
 * node: A concrete node in a distributed system. Employs a variadic
 * template to extend from arbitrarily many Policy objects, each of which
 * implement a part of the distributed system. For example, a Policy might
 * be a udp transport protocol, or a feature might be a ring-based failure
 * detector.
 *
 * The Policy objects employ the Curiously Recurring Template Pattern
 * (CRTP) so that they may be aware of the node class which extends from
 * them.
 */
template <size_t MaxSize, template <size_t, class> class ... Features>
class node : public abstract_node<MaxSize>, public Features<MaxSize, node<MaxSize, Features...>>... {
    public:
        using abstract_node<MaxSize>::handle_message;
        typedef typename abstract_node<MaxSize>::buffer_type buffer_type;
        typedef typename abstract_node<MaxSize>::handler_func handler_func;

        /**
         * Constructs a node object, operating on the given port.
         *
         * @param service The io_service to schedule events on.
         * @param port The port to listen on.
         */
        node( boost::asio::io_service & service, uint16_t port ) :
            Features<MaxSize, node<MaxSize, Features...>>( service, port )... {
            LOG_FUNCTION_START();
        }

        /**
         * Provided so that nodes that derive from this may override.
         */
        virtual ~node() { 
            LOG_FUNCTION_START();
        }
};

}
#endif
