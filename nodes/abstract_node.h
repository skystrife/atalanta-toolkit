/**
 * @file abstract_node.h
 * Definition of an abstract node in a distributed system.
 */
#ifndef _ABSTRACT_NODE_H_
#define _ABSTRACT_NODE_H_

#include <stdint.h>

#include <algorithm>
#include <boost/asio.hpp>

#include "message_protocols/msg_buffer.h"
#include "message_protocols/message_protocol.h"
#include "log/logger.h"

namespace atalanta {

/**
 * abstract_node: Represents an interface for nodes in a distributed
 * system.
 */
template <size_t MaxSize>
class abstract_node {
    public:
        typedef typename message_protocol<MaxSize>::endpoint endpoint;
        typedef std::shared_ptr<msg_buffer> buffer_ptr;
        typedef std::function<void( buffer_ptr, const endpoint &  )> handler_func;
        typedef std::array<char, MaxSize> buffer_type;

        /**
         * Adds a new message type to the set of handlers.
         *
         * @param func The callback to be executed for the new message
         *  type.
         * @return The id that was assigned to the new message type.
         */
        uint8_t add_message( const handler_func & func ) {
            LOG_FUNCTION_START();
            LOG( trace ) << "new handler for message id: " << _handlers.size() << ENDLG;
            _handlers.push_back( func );
            return static_cast<uint8_t>( _handlers.size() - 1 );
        }

        /**
         * Callback for when the message protocol handles a message. Calls
         * the appropriate handler, passing along a shared pointer to the
         * buffer.
         *
         * @param buffer A shared_ptr to the buffer containing the message.
         * @param num_bytes The number of bytes in the message.
         */
        void handle_message( const buffer_type & buffer, size_t num_bytes,
                const endpoint & sender ) {
            LOG_FUNCTION_START();
            size_t idx = static_cast<uint8_t>( buffer[0] );
            auto buffer_copy = std::make_shared<msg_buffer>();
            buffer_copy->get_raw_buffer().resize( num_bytes );
            std::copy( buffer.begin(), buffer.begin() + num_bytes,
                    buffer_copy->get_raw_buffer().begin() );
            _handlers[idx]( buffer_copy, sender );
        }

    private:
        /**
         * List of message handlers.
         */
        std::vector<handler_func> _handlers;
};

}
#endif
