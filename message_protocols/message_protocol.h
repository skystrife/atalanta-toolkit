/**
 * @file message_protocol.h
 * A definition for an abstract message protocol feature.
 */
#ifndef _MESSAGE_PROTOCOL_H_
#define _MESSAGE_PROTOCOL_H_

#include <array>
#include <memory>
#include <string>
#include <vector>
#include <iostream>

#include <boost/asio.hpp>

#include "log/logger.h"

namespace atalanta {

namespace internal {
    /**
     * Represents an endpoint for communication: contains an ip and a port.
     */
    class _endpoint {
        public:
            /**
             * Constructs a new endpoint with the given ip and port.
             *
             * @param ip The ip address for the endpoint.
             * @param port The port for the endpoint.
             */
            _endpoint( const std::string & ip, int port ) : _ip( ip ), _port( port ) {
                // nothing
            }

            /**
             * Gets the ip for the endpoint.
             */
            const std::string & ip() const {
                return _ip;
            }

            /**
             * Sets the ip for the endpoint.
             *
             * @param ip The desired ip.
             */
            void ip( const std::string & ip ) {
                _ip = ip;
            }

            /**
             * Gets the port for the endpoint.
             */
            int port() const {
                return _port;
            }

            /**
             * Sets the port for the endpoint.
             *
             * @param port The desired port.
             */
            void port( int port ) {
                _port = port;
            }

            /**
             * Determines if two endpoints are equivalent.
             *
             * @param other The endpoint to compare against.
             */
            bool operator==( const _endpoint & other ) const {
                return ip() == other.ip() && port() == other.port();
            }

            /**
             * Writes a string form of this endpoint to a stream.
             *
             * @param os The ostream to write to.
             */
            friend inline std::ostream & operator<<( std::ostream & os, const _endpoint & ep ) {
                return os << ep.ip() << ":" << ep.port();
            }
        private:
            /**
             * The ip for this endpoint.
             */
            std::string _ip;

            /**
             * The port for this endpoint.
             */
            int _port;
    };
}

/**
 * message_protocol: A base class that all message sending protocols should
 * extend from. Defines some basic interfaces for handling messages.
 */
template <size_t MaxSize>
class message_protocol {
    public:
        /**
         * Endpoints represent targets for messages.
         */
        typedef internal::_endpoint endpoint;

        typedef std::shared_ptr<msg_buffer> buffer_ptr;

        /**
         * Sends the given message to the endpoint.
         *
         * @param target The desired recipient for the message.
         * @param buffer A buffer_ptr containing the data to send.
         */
        virtual void send_message( const endpoint & target,
                buffer_ptr buffer ) = 0;

        /**
         * Sends a multicast message to all desired recipients.
         *
         * @param targets The list of recipients for the multicast.
         * @param buffer The data to be sent.
         */
        virtual void multicast( const std::vector<endpoint> & targets,
                buffer_ptr buffer ) {
            // default to reliable multicast
            reliable_multicast( targets, buffer );
        };

        /**
         * Sends a reliable multicast to all desired recipients.
         *
         * @param targets The list of recipients for the multicast.
         * @param buffer The data to be sent.
         */
        virtual void reliable_multicast( const std::vector<endpoint> & targets,
                buffer_ptr buffer ) = 0;

        /**
         * message handler: Invoked when a message has been received.
         * Should invoke the correct node message handler.
         */
        virtual void handle_message(
                const boost::system::error_code & error, 
                size_t bytes_received ) = 0;

        /**
         * Virtual destructor in case derived protocols have dynamic memory
         * to free.
         */
        virtual ~message_protocol() {
            // nothing
        }

        /**
         * Helper function to create an endpoint.
         *
         * @param ip The desired ip address of the endpoint.
         * @param port The desired port of the endpoint.
         */
        virtual endpoint create_endpoint( const std::string & ip, int port ) {
            return endpoint( ip, port );
        }
};

}

namespace std {
    template <>
    struct hash<atalanta::internal::_endpoint> {
        size_t operator()( const atalanta::internal::_endpoint & to_hash ) const {
            std::stringstream ss;
            ss << to_hash.ip() << ":" << to_hash.port();
            return std::hash<std::string>()( ss.str() );
        }
    };
}
#endif
