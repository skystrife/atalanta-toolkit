/**
 * @file udp_protocol.h
 * Definition of a udp-based message sending protocol.
 */
#ifndef _UDP_PROTOCOL_H_
#define _UDP_PROTOCOL_H_

#include <stdint.h>

#include <unordered_map>
#include "message_protocols/message_protocol.h"

namespace atalanta {

/**
 * udp_protocol: A message sending protocol over UDP.
 */
template <size_t MaxSize, class Node>
class udp_protocol : public virtual message_protocol<MaxSize> {
    public:
        typedef typename message_protocol<MaxSize>::endpoint endpoint;
        typedef boost::asio::ip::udp::endpoint udp_endpoint;
        typedef std::shared_ptr<msg_buffer> buffer_ptr;

        /**
         * Creates the protocol on the given service and port.
         *
         * @param service The io_service to schedule events on.
         * @param port The port number to listen for connections on.
         */
        udp_protocol( boost::asio::io_service & service, uint16_t port );

        /**
         * Sends to the given target the desired buffer.
         *
         * @param target The desired recipient of the message.
         * @param buffer The data to be sent in the message.
         */
        virtual void send_message( const endpoint & target,
                buffer_ptr buffer );

        /**
         * Multicasts a message to all desired recipients.
         *
         * @param targets The list of recipients for the multicast.
         * @param buffer The data to be sent in the multicast.
         */
        virtual void multicast( const std::vector<endpoint> & targets,
                buffer_ptr buffer );

        /**
         * "reliable sending": Implemented as a hack, considering this is a
         * udp transport. Message resending is attempted up to 5 times,
         * after which delivery is abandoned. The assumption here is that
         * the target node would have been detected as having failed
         * **before** the 5 delivery re-attempts, so this is relatively
         * safe.
         *
         * @param target The desired recipient for the message.
         * @param buffer The data to be sent in the message.
         */
        virtual void reliable_send( const endpoint & target,
                buffer_ptr buffer );

        /**
         * "reliable multicast": Implemented as a hack, using the above
         * reliable send hack, considering this is a udp transport.
         *
         * @param targets The desired recipients for the multicast.
         * @param buffer The data to be sent in the message.
         */
        virtual void reliable_multicast( const std::vector<endpoint> & targets,
                buffer_ptr buffer );

        /**
         * Message handler: invoked when an entire message has been
         * received.
         *
         * @param error An error if one occurred while receiving.
         * @param bytes_received The number of bytes that were received.
         */
        virtual void handle_message( 
                const boost::system::error_code & error, 
                size_t bytes_received );

    private:
        /**
         * Schedules a message resending event for the "reliable" sending
         * methods.
         *
         * @param target The desired recipient for the message.
         * @param buffer The data to be sent in the message.
         * @param try_number The number of times redelivery has been
         *  attempted.
         */
        void schedule_resend( const endpoint & target,
                buffer_ptr buffer, int try_number );

        /**
         * Resends a message.
         *
         * @param target The desired recipient for the message.
         * @param buffer The data to be sent in the message.
         * @param error An error if one occurred in the timer.
         */
        void resend_message( const endpoint & target,
                buffer_ptr buffer, int try_number, 
                const boost::system::error_code & error );

        /**
         * Sends an acknowledgement message for the given message id.
         *
         * @param id The message id that an ack should be sent for.
         */
        void send_ack( uint8_t id );

        /**
         * Handles an acknowledgement message for the given message id.
         *
         * @param id The message id for the received acknowledgement.
         */
        void handle_ack( uint8_t id );

        /**
         * Schedules an async event to receive a message.
         */
        void start_receive();

        /**
         * Callback for when a message has been sent.
         *
         * @param buffer The buffer that was being sent.
         * @param error An error if one occurred while sending.
         * @param bytes_sent The number of bytes sent.
         */
        void handle_send( buffer_ptr buffer, 
                const boost::system::error_code & error, size_t bytes_sent );

        /**
         * The udp socket to send and receive messages on.
         */
        boost::asio::ip::udp::socket _socket;
        
        /**
         * The endpoint of the sender of the last message.
         */
        boost::asio::ip::udp::endpoint _sender_endpoint;

        /**
         * The buffer to receive messages into.
         */
        std::array<char, MaxSize> _receive_buffer;

        /**
         * Timers used for scheduling "reliable sending" of messages.
         */
        std::unordered_map<std::string, std::shared_ptr<boost::asio::deadline_timer>> 
            _reliable_message_timers;
};

}
#include "message_protocols/udp_protocol.cpp"
#endif
