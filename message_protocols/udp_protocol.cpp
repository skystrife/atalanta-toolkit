/**
 * @file udp_protocol.cpp
 * Implementation of a udp-based message sending protocol.
 */
using namespace std::placeholders;

namespace atalanta {

template <size_t MaxSize, class Node>
udp_protocol<MaxSize, Node>::udp_protocol( 
        boost::asio::io_service & service, uint16_t port ) 
    : _socket( service, boost::asio::ip::udp::endpoint(
                boost::asio::ip::udp::v4(), port ) ) {
    //LOG_FUNCTION_START();
    start_receive();
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::start_receive() {
    //LOG_FUNCTION_START();
    _socket.async_receive_from( 
            boost::asio::buffer( _receive_buffer.data(), _receive_buffer.size() ),
            _sender_endpoint, 
            std::bind( &udp_protocol<MaxSize, Node>::handle_message, this,
                _1, _2 ) 
        );
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::handle_message(
        const boost::system::error_code & error, size_t bytes_received ) {
    LOG_FUNCTION_START();
    if( !error ) {

        uint8_t msg_id = static_cast<uint8_t>( _receive_buffer[0] );
        LOG( trace ) << "message id: " << static_cast<size_t>( msg_id ) << ENDLG;

        if( msg_id == 255 ) { // ack message
            handle_ack( static_cast<uint8_t>( _receive_buffer[1] ) );
        } else {
            if( msg_id >= 200 ) {
                send_ack( msg_id );
                _receive_buffer[0] -= 200;
            }

            endpoint ep( _sender_endpoint.address().to_string(),
                    _sender_endpoint.port() );
            
            static_cast<Node *>(this)->handle_message( _receive_buffer,
                    bytes_received, ep );
        }
        start_receive();
    } else {
        LOG( error ) << error.message() << ENDLG;
    }
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::send_message( const endpoint & target,
        buffer_ptr buffer ) {
    LOG_FUNCTION_START();
    _socket.async_send_to(
            boost::asio::buffer( buffer->get_raw_buffer() ),
            boost::asio::ip::udp::endpoint( boost::asio::ip::address::from_string( target.ip() ), target.port() ),
            std::bind( &udp_protocol<MaxSize, Node>::handle_send, this, buffer, _1, _2 )
        );
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::multicast( 
        const std::vector<endpoint> & targets, 
        buffer_ptr buffer ) {
    for( const endpoint & target : targets )
        send_message( target, buffer );
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::handle_send(
        buffer_ptr buffer, 
        const boost::system::error_code & error, size_t num_sent ) {
    //LOG_FUNCTION_START();
    if( error ) {
        LOG( error ) << error.message() << ENDLG;
    }
    if( buffer->get_raw_buffer().size() != num_sent ) {
        LOG( error ) << "did not send whole buffer (sent " << num_sent 
            << ", buffer was " << buffer->get_raw_buffer().size() << ")" << ENDLG;
    }
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::reliable_multicast( 
        const std::vector<endpoint> & targets, 
        buffer_ptr buffer ) {
    LOG_FUNCTION_START();
    LOG( warning ) << "R-Multicast not yet implemented, using B-Multicast "
        "with reliable_send" << ENDLG;
    for( const endpoint & target : targets )
        reliable_send( target, buffer );
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::reliable_send( const endpoint & target,
        buffer_ptr buffer ) {
    LOG_FUNCTION_START();
    if( static_cast<uint8_t>( buffer->get_raw_buffer()[0] ) < 200 )
        buffer->get_raw_buffer()[0] += 200; // hack: msg_id > 200 denotes messages needing
                             // acknowledgement.
                             // assuming nobody is going to
                             // want 200 distinct message types, and if
                             // they do they're bordering on needing to
                             // redesign the whole protocol anyway
    send_message( target, buffer );
    schedule_resend( target, buffer, 1 );
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::schedule_resend( const endpoint & target,
        buffer_ptr buffer, int try_number ) {
    LOG_FUNCTION_START();
    std::stringstream ss;
    ss << target.ip() << ":" << target.port() << ":" 
        << static_cast<size_t>( static_cast<uint8_t>( buffer->get_raw_buffer()[0] ) );
    if( try_number == 5 ) {
        LOG( error ) << "Abandoning resend attempts after 5 tries" << ENDLG;
        std::stringstream ss;
        if( _reliable_message_timers[ ss.str() ] != NULL )
            _reliable_message_timers[ ss.str() ]->cancel();
        _reliable_message_timers.erase( ss.str() );
    } else {
        auto timer = std::make_shared<boost::asio::deadline_timer>( _socket.get_io_service() );
        timer->expires_from_now( boost::posix_time::seconds( 2 ) );
        timer->async_wait( 
                std::bind( &udp_protocol<MaxSize, Node>::resend_message,
                    this, target, buffer, try_number + 1, _1 )
            );
        LOG( trace ) << "adding timer with key: " << ss.str() << ENDLG;
        _reliable_message_timers[ ss.str() ] = timer;
    }
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::resend_message( const endpoint & target,
        buffer_ptr buffer, int try_number, 
        const boost::system::error_code & error ) {
    LOG_FUNCTION_START();
    if( !error ) {
        send_message( target, buffer );
        schedule_resend( target, buffer, try_number );
    } else {
        if( error.value() != boost::system::errc::operation_canceled )
            LOG( error ) << error.message() << ENDLG;
    }
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::send_ack( uint8_t id ) {
    LOG_FUNCTION_START();
    auto buffer = std::make_shared<msg_buffer>();
    buffer->set_msg_id( 255 ); // hack: acknowledgement messages have id 255
    *buffer << id;
    LOG( trace ) << "sending ack with id: " 
        << static_cast<size_t>( static_cast<uint8_t>( buffer->get_raw_buffer()[1] ) ) 
        << ENDLG;
    _socket.async_send_to(
            boost::asio::buffer( buffer->get_raw_buffer() ),
            _sender_endpoint,
            std::bind( &udp_protocol<MaxSize, Node>::handle_send, this, buffer, _1, _2 )
        );
}

template <size_t MaxSize, class Node>
void udp_protocol<MaxSize, Node>::handle_ack( uint8_t id ) {
    LOG_FUNCTION_START();
    std::stringstream ss;
    ss << _sender_endpoint.address().to_string() << ":" 
        << _sender_endpoint.port() << ":" << static_cast<size_t>( id );
    LOG( trace ) << "cancelling timer with key: " << ss.str() << ENDLG;
    if( _reliable_message_timers[ ss.str() ] != NULL )
        _reliable_message_timers[ ss.str() ]->cancel();
    _reliable_message_timers.erase( ss.str() );
}

}
