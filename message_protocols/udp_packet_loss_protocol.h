/**
 * @file udp_packet_loss_protocol.h
 * Definition for a packet-losing udp protocol.
 */
#ifndef _UDP_PL_PROTOCOL_
#define _UDP_PL_PROTOCOL_

#include <random>

#include "message_protocols/udp_protocol.h"

namespace atalanta {

/**
 * udp_packet_loss_protocol: A simulation protocol built on top of udp that
 * simulates packet loss for running tests for robustness in poor
 * connectivity conditions.
 */
template <size_t MaxSize, class Node>
class udp_packet_loss_protocol : public udp_protocol<MaxSize, Node> {
    public:
        /**
         * Creates a new protocol on the given io service and port.
         *
         * @param service The io_service to schedule events on.
         * @param port The port to listen for connections on.
         */
        udp_packet_loss_protocol( boost::asio::io_service & service, uint16_t port ) :
            udp_protocol<MaxSize, Node>( service, port ), 
            _generator( time(NULL) ), _distribution( 1, 100 ), _percent( 0 ) {
            // nothing
        }

        /**
         * Sets the percentage packet loss for this protocol.
         *
         * @param percent The percentage of packets to drop (as an integer,
         *  0-100).
         */
        void percent_loss( int percent ) {
            _percent = percent;
        }

        /**
         * Overloaded message handler. Picks a number between 1 and 100: if
         * it is less than the percentage packet loss number, the message
         * is dropped. Otherwise, the base class message handler is called
         * as ususal.
         *
         * @param error An error if one occurred when receiving the
         *  message.
         * @param bytes_received The number of bytes that were received in
         *  this message.
         */
        virtual void handle_message( const boost::system::error_code & error, 
                size_t bytes_recieved ) {
            int draw = _distribution( _generator );
            if( draw <= _percent ) {
                LOG( info ) << "dropping packet (draw was " << draw << ")" << ENDLG;
                udp_protocol<MaxSize, Node>::start_receive();
            } else {
                udp_protocol<MaxSize, Node>::handle_message( error, bytes_recieved );
            }
        }

    private:
        /**
         * The random engine used for creating random numbers.
         */
        std::default_random_engine _generator;
        /**
         * The distribution to draw our random numbers from.
         */
        std::uniform_int_distribution<int> _distribution;
        /**
         * The percent of packets to drop.
         */
        int _percent;
};

}
#endif
