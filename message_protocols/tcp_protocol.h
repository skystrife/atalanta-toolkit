/**
 * @file tcp_protocol.h
 * A definition for a message protocol feature over tcp.
 */
#ifndef _TCP_PROTOCOL_H_
#define _TCP_PROTOCOL_H_

#include <unordered_map>

#include "message_protocols/message_protocol.h"

namespace atalanta {

/**
 * tcp_protocol: A message protocol feature that communicates all messages
 * via TCP.
 */
template <size_t MaxSize, class Node>
class tcp_protocol : public virtual message_protocol<MaxSize> {
    public:
        typedef typename message_protocol<MaxSize>::endpoint endpoint;
        typedef std::shared_ptr<msg_buffer> buffer_ptr;
        typedef boost::asio::ip::tcp::socket tcp_socket;
        typedef std::shared_ptr<tcp_socket> socket_ptr;

        /**
         * Constructs the tcp_protocol feature on the given io_service and
         * port number.
         *
         * @param service The io_service to scheudle async events on.
         * @param port The port number to listen on.
         */
        tcp_protocol( boost::asio::io_service & service, uint16_t port )
            : _acceptor( service, boost::asio::ip::tcp::endpoint( 
                        boost::asio::ip::tcp::v4(), port 
                    ) 
                ), _node( static_cast<Node *>( this ) ) {
            LOG_FUNCTION_START();
            start_accept();
        }

        /**
         * Sends the given message to the endpoint.
         *
         * @param target The desired recipient for the message.
         * @param buffer A buffer_ptr containing the data to send.
         */
        virtual void send_message( const endpoint & target, 
                buffer_ptr buffer ) {
            LOG_FUNCTION_START();
            auto it = _endpoint_map.find( target );
            if( it != _endpoint_map.end() ) {
                it->second.send_message( buffer );
            } else {
                establish_connection( target, 
                        std::bind( &tcp_protocol<MaxSize, Node>::send_message,
                            this, target, buffer ) );
            }
        }

                
        /**
         * Sends the given message to the endpoint reliably.
         *
         * @param target The desired recipient for the message.
         * @param buffer A buffer_ptr containing the data to send.
         */
        virtual void reliable_send( const endpoint & target,
                buffer_ptr buffer ) {
            LOG_FUNCTION_START();
            // we're using TCP, so our normal send is also reliable
            tcp_protocol::send_message( target, buffer );
        }

        /**
         * Sends a multicast message to all desired recipients.
         *
         * @param targets The list of recipients for the multicast.
         * @param buffer The data to be sent.
         */
        virtual void multicast( 
                const std::vector<endpoint> & targets, buffer_ptr buffer ) {
            LOG_FUNCTION_START();
            for( const endpoint & ep : targets )
                tcp_protocol::send_message( ep, buffer );
        }

        /**
         * Sends a reliable multicast message to all desired recipients.
         *
         * @param targets The list of recipients for the multicast.
         * @param buffer The data to be sent.
         */
        virtual void reliable_multicast(
                const std::vector<endpoint> & targets, buffer_ptr buffer ) {
            LOG( warning ) << "R-Multicast not yet implemented, using B-Multicast" << ENDLG;
            tcp_protocol::multicast( targets, buffer );
        }

        /**
         * Overloaded message handler: never invoked, because control of
         * message handling is given to individual connection objects.
         */
        virtual void handle_message( 
                const boost::system::error_code & error, 
                size_t bytes_received ) {
            LOG( fatal ) << "this is not implemented" << ENDLG;
            std::terminate();
        }

    private:

        /**
         * connection: Encapsulates a single connection between nodes in
         * the network.
         */
        class connection {
            public:
                /**
                 * Constructs a new connection from the given node, to the
                 * node connected over the given socket.
                 *
                 * @param node A pointer to the current node.
                 * @param socket A pointer to the socket used for
                 *  communication.
                 * @param ep The endpoint this connection communicates
                 *  with. (Note that this is not the same as the endpoint
                 *  of the socket itself, because it will have been
                 *  resolved to a different port by the accepting node).
                 */
                connection( Node * node, socket_ptr socket, const endpoint & ep )
                    : _node( node ),
                    _protocol( static_cast<tcp_protocol *>( node ) ),
                    _socket( socket ),
                    _ep( ep ) {
                    LOG_FUNCTION_START();
                    start();
                }

                /**
                 * Tears down the connection.
                 */
                ~connection() {
                    LOG_FUNCTION_START();
                }

                /**
                 * Starts the connection.
                 */
                void start() {
                    LOG_FUNCTION_START();
                    start_receive_size();
                }

                /**
                 * Begins receiving the size of the next message.
                 */
                void start_receive_size() {
                    LOG_FUNCTION_START();
                    boost::asio::async_read( *_socket,
                            boost::asio::buffer( _size_buffer.data(), 2 ),
                            std::bind( &connection::handle_msg_size, this,
                                _1, _2 )
                        );
                }

                /**
                 * Handles the receipt of the size of the next message.
                 *
                 * @param error An error if one occurred while reading.
                 * @param bytes_read The number of bytes read.
                 */
                void handle_msg_size( 
                        const boost::system::error_code & error, 
                        size_t bytes_read ) {
                    LOG_FUNCTION_START();
                    LOG( trace ) << "bytes read: " << bytes_read << ENDLG;
                    if( !error ) {
                        uint16_t msg_size = 0;
                        msg_size |= static_cast<uint8_t>( _size_buffer[0] ) << 8;
                        msg_size |= static_cast<uint8_t>( _size_buffer[1] );
                        start_receive( msg_size );
                    } else {
                        LOG( error ) << error.message() << ENDLG;
                        _protocol->remove_socket( _ep );
                    }
                }

                /**
                 * Starts receiving the next message.
                 *
                 * @param msg_size The size of the message to receive.
                 */
                void start_receive( uint16_t msg_size ) {
                    LOG_FUNCTION_START();
                    boost::asio::async_read( *_socket,
                            boost::asio::buffer( _buffer.data(), msg_size ),
                            std::bind( &connection::handle_message, this,
                                _1, _2 )
                        );
                }

                /**
                 * Handles a message when it has been received.
                 *
                 * @param error An error if one occurred while reading.
                 * @param bytes_read The number of bytes that were read.
                 */
                void handle_message( 
                        const boost::system::error_code & error, 
                        size_t bytes_read ) {
                    LOG_FUNCTION_START();
                    if( !error ) {
                        _node->handle_message( _buffer, bytes_read, _ep );
                        start_receive_size();
                    } else {
                        LOG( error ) << error.message() << ENDLG;
                        _protocol->remove_socket( _ep );
                    }
                }

                /**
                 * Sends the given message.
                 *
                 * @param buffer A pointer to the msg_buffer to send.
                 */
                void send_message( buffer_ptr buffer ) {
                    LOG_FUNCTION_START();
                    uint16_t msg_size = buffer->size();
                    _send_size_buffer[0] = static_cast<char>( ( msg_size >> 8 ) & 0xFF );
                    _send_size_buffer[1] = static_cast<char>( msg_size & 0xFF );
                    boost::asio::async_write( *_socket, 
                            boost::asio::buffer( _send_size_buffer.data(), 2 ),
                            std::bind( &connection::handle_send_msg_size, this,
                                buffer, _1, _2 )
                        );
                }

                /**
                 * Callback for when the message size has been sent.
                 * Continues sending the message.
                 *
                 * @param buffer The buffer containing the message to send.
                 * @param error An error if one occurred while sending.
                 * @param bytes_sent The number of bytes that were sent.
                 */
                void handle_send_msg_size( buffer_ptr buffer, 
                        const boost::system::error_code & error, 
                        size_t bytes_sent ) {
                    LOG_FUNCTION_START();
                    if( !error ) {
                        boost::asio::async_write( *_socket,
                                boost::asio::buffer( buffer->get_raw_buffer() ),
                                std::bind( &connection::handle_write, this,
                                    buffer, _1, _2 )
                            );
                    } else {
                        LOG( error ) << error.message() << ENDLG;
                        _protocol->remove_socket( _ep );
                    }
                }

                /**
                 * Callback for when data is written to the socket.
                 *
                 * @param error An error if one occurred while writing.
                 * @param bytes_written The number of bytes that were
                 *  written.
                 */
                void handle_write( buffer_ptr buffer, 
                        const boost::system::error_code & error, 
                        size_t bytes_written ) {
                    LOG_FUNCTION_START();
                    if( error ) {
                        LOG( error ) << error.message() << ENDLG;
                        _protocol->remove_socket( _ep );
                    }
                }
            private:
                /**
                 * A pointer to the node this connection belongs to.
                 */
                Node * _node;

                /**
                 * A pointer to the protocol that created this connection.
                 */
                tcp_protocol * _protocol;

                /**
                 * A pointer to the tcp_socket used for communication.
                 */
                socket_ptr _socket;

                /**
                 * The endpoint of this connection.
                 */
                endpoint _ep;

                /**
                 * The buffer used for receiving messages on this
                 * connection.
                 */
                std::array<char, MaxSize> _buffer;

                /**
                 * The buffer used for receiving message sizes on this
                 * connection.
                 */
                std::array<char, 2> _size_buffer;

                /**
                 * The buffer used for sending message sizes on this
                 * connection.
                 */
                std::array<char, 2> _send_size_buffer;
        };

        /**
         * Begins listening for new tcp connections.
         */
        void start_accept() {
            LOG_FUNCTION_START();
            auto socket = std::make_shared<tcp_socket>(
                    _acceptor.get_io_service() 
                );
            _acceptor.async_accept( *socket, 
                    std::bind( &tcp_protocol<MaxSize, Node>::handle_accept,
                        this, socket, _1 ) );
        }

        /**
         * Sends the current port over the given socket. This is used on
         * the accepting end to determine the accepting port of the
         * connecting socket. This allows us to reuse the connection for
         * both sending and receiving.
         *
         * @param socket A pointer to the tcp_socket to send the port over.
         * @param callback A callback to be invoked when finished.
         */
        void send_port( socket_ptr socket, std::function<void()> callback ) {
            LOG_FUNCTION_START();
            uint16_t port = static_cast<uint16_t>( _acceptor.local_endpoint().port() );
            _send_port_buffer[0] = static_cast<char>( ( port >> 8 ) & 0xFF );
            _send_port_buffer[1] = static_cast<char>( port & 0xFF );
            boost::asio::async_write( *socket,
                    boost::asio::buffer( _send_port_buffer.data(), 2 ),
                    std::bind( &tcp_protocol<MaxSize, Node>::handle_send_port,
                        this, _1, _2, callback )
                );
        }

        void handle_send_port( const boost::system::error_code & error,
                size_t bytes_sent, std::function<void()> callback ) {
            LOG_FUNCTION_START();
            if( !error )
                callback();
            else
                LOG( error ) << error.message() << ENDLG;
        }
        /**
         * Handler invoked when a new tcp connection has been established.
         *
         * @param socket The socket_ptr for the socket that was created for
         *  this new connection.
         * @param error An error if one occurred while accepting the
         *  connection.
         */
        void handle_accept( socket_ptr socket, 
                const boost::system::error_code & error ) {
            LOG_FUNCTION_START();
            if( !error ) {
                LOG( trace ) << "accepted to: " << socket->remote_endpoint().port() << ENDLG;
                receive_port( socket );
                start_accept();
            } else {
                LOG( error ) << error.message() << ENDLG;
            }
        }

        /**
         * Handler invoked when the port has been sent over the socket.
         *
         * @param buffer The buffer used to send the port.
         * @param error An error if one occurred while sending.
         * @param bytes_sent The number of bytes that were sent.
         * @param callback A callback to be invoked when finished.
         */
        void handle_send( buffer_ptr buffer, 
                const boost::system::error_code & error, size_t bytes_sent,
                std::function<void()> callback ) {
            LOG_FUNCTION_START();
            if( !error )
                callback();
            else
                LOG( error ) << error.message() << ENDLG;
        }

        /**
         * Receives the port of the connecting node over the given socket.
         *
         * @param socket A pointer to the tcp_socket to listen for the port
         *  over.
         */
        void receive_port( socket_ptr socket ) {
            LOG_FUNCTION_START();
            boost::asio::async_read( *socket,
                    boost::asio::buffer( _recv_port_buffer.data(), 2 ),
                    std::bind( &tcp_protocol<MaxSize, Node>::handle_port,
                        this, socket, _1, _2 )
                );
        }

        /**
         * Handler invoked when the port of a connecting node has been
         * received.
         *
         * @param socket A pointer to the tcp_socket used for
         *  communicating.
         * @param error An error if one occurred while reading.
         * @param bytes_read The number of bytes that were read.
         */
        void handle_port( socket_ptr socket, 
                const boost::system::error_code & error, size_t bytes_read ) {
            LOG_FUNCTION_START();
            if( !error ) {
                uint16_t port = 0;
                port |= static_cast<uint8_t>( _recv_port_buffer[0] ) << 8;
                port |= static_cast<uint8_t>( _recv_port_buffer[1] );
                endpoint ep( socket->remote_endpoint().address().to_string(), port );
                insert_socket( ep, socket );
            } else {
                LOG( error ) << error.message() << ENDLG;
            }
        }

        /**
         * Attempts to establish a connection to the given target.
         *
         * @param target The destination endpoint to establish a connection
         *  to.
         * @param callback A callback to be invoked when finished.
         */
        void establish_connection( const endpoint & target,
                std::function<void()> callback ) {
            auto socket = std::make_shared<tcp_socket>( _acceptor.get_io_service() );
            boost::asio::ip::tcp::endpoint ep(
                    boost::asio::ip::address::from_string( target.ip() ),
                    target.port() );
            socket->async_connect( ep,
                    std::bind( &tcp_protocol<MaxSize, Node>::handle_connect,
                        this, socket, callback, _1 ) );
        }

        /**
         * Handler invoked when a connection has been established.
         *
         * @param socket A pointer to the tcp_socket that was established.
         * @param callback A callback to invoke when complete.
         * @param error An error if one occurred while connecting.
         */
        void handle_connect( socket_ptr socket, std::function<void()> callback,
                const boost::system::error_code & error ) {
            LOG_FUNCTION_START();
            if( !error ) {
                LOG( trace ) << "connected to: " << socket->remote_endpoint().port() << ENDLG;
                endpoint ep( socket->remote_endpoint().address().to_string(), 
                        socket->remote_endpoint().port() );
                insert_socket( ep, socket );
                send_port( socket, callback );
            } else {
                LOG( error ) << error.message() << ENDLG;
            }
        }

        /**
         * Inserts the given socket into the connection list.
         *
         * @param ep The endpoint whose socket is being inserted.
         * @param socket A pointer to the tcp_socket for this endpoint.
         */
        void insert_socket( const endpoint & ep, socket_ptr socket ) {
            LOG_FUNCTION_START();
            auto it = _endpoint_map.find( ep );
            if( it != _endpoint_map.end() ) {
                LOG( trace ) << "already an endpoint for " << ep 
                    << ", inserting with actual destination endpoint..." << ENDLG;
                // this is a hack to work around establishing a tcp
                // connection to the current process---other than that
                // case, this branch should never be taken.
                _endpoint_map.emplace(
                        std::piecewise_construct,
                        std::forward_as_tuple(
                            socket->remote_endpoint().address().to_string(),
                            socket->remote_endpoint().port()
                        ),
                        std::forward_as_tuple( _node, socket, ep )
                    );
            } else {
                LOG( trace ) << "no endpoint for " << ep << " found" << ENDLG;
                LOG( trace ) << "map size currently is " << _endpoint_map.size() << ENDLG;
                _endpoint_map.emplace(
                        std::piecewise_construct, std::forward_as_tuple( ep ), 
                        std::forward_as_tuple( _node, socket, ep )
                    );
            }
        }

        /**
         * Removes a socket from the connection list.
         *
         * @param ep The endpoint representing the connection to remove.
         */
        void remove_socket( const endpoint & ep ) {
            LOG_FUNCTION_START();
            _endpoint_map.erase( ep );
        }

        /**
         * Acceptor class to handle new socket connections.
         */
        boost::asio::ip::tcp::acceptor _acceptor;

        /**
         * Node class pointer to ourselves.
         */
        Node * _node;

        /**
         * Map of endpoints to connections.
         */
        std::unordered_map<endpoint, connection> _endpoint_map;

        std::array<char, 2> _send_port_buffer;
        std::array<char, 2> _recv_port_buffer;
};

}


#endif
