/**
 * @file msg_buffer.h
 * A definition for a generic message buffer that supports iostream-like
 * operations.
 */
#ifndef _MSG_BUFFER_H_
#define _MSG_BUFFER_H_
#include <vector>
#include "log/logger.h"
namespace atalanta {

/**
 * msg_buffer: A message buffer interface for creating messages to be sent
 * over the network. This is a sparse-format buffer, meaning that it is
 * simply serializing data byte-by-byte and not using any special
 * compression techniques. This is probably fine for most use cases, but
 * compression techniques are a good consideration as well.
 */
class msg_buffer {
    public:
        /**
         * Creates a new empty msg_buffer.
         */
        msg_buffer() : _buffer( 1 ), _pos ( 1 ) {
            // nothing
        }

        /**
         * Sets the id of the message this buffer is for.
         *
         * @param msg_id The desired message id.
         */
        void set_msg_id( uint8_t msg_id ) {
            _buffer[ 0 ] = msg_id;
        }

        /**
         * Gets the id of the message for this buffer.
         */
        uint8_t msg_id() {
            return static_cast<uint8_t>( _buffer[0] );
        }

        /**
         * Generic streaming operator: writes the parameter to the stream.
         * Should work for all integer types, but you may have to
         * specialize this function if you want to serialize an entire
         * class automatically as opposed to sending its components.
         *
         * @param to_write The data to be written to the msg_buffer.
         */
        template <class T>
        msg_buffer & operator<<( const T & to_write ) {
            for( size_t i = 0; i < sizeof( T ); ++i )
                _buffer.push_back( static_cast<char>( ( to_write >> ( ( sizeof( T ) - 1 - i ) * 8 ) ) & 0xFF ) );
            return *this;
        }

        /**
         * Generic streaming operator: reads from the stream. Should work
         * for all integer types, but you may have to specialize this
         * function if you want to deserialize and entire class
         * automatically as opposed to receiving its individual components.
         *
         * @param to_read Where the place the data read from the stream.
         */
        template <class T>
        msg_buffer & operator>>( T & to_read ) {
            to_read = 0;
            for( size_t i = 0; i < sizeof( T ); ++i ) {
                uint8_t curr = static_cast<uint8_t>( _buffer[ _pos ] );
                to_read |= ( curr << ( sizeof( T ) - 1 - i ) * 8 );
                ++_pos;
            }
            return *this;
        }

        /**
         * Grabs the underlying buffer.
         */
        std::vector<char> & get_raw_buffer() {
            return _buffer;
        }

        /**
         * Gets the current size of the buffer.
         */
        size_t size() const {
            return _buffer.size();
        }

    private:
        /**
         * The underlying buffer.
         */
        std::vector<char> _buffer;

        /**
         * The read cursor into the buffer.
         */
        size_t _pos;
};

}
#endif
