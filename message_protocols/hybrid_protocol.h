/**
 * @file hybrid_protocol.h
 * A definition for a message protocol that uses both UDP and TCP.
 */
#ifndef _HYBRID_PROTOCOL_H_
#define _HYBRID_PROTOCOL_H_

#include "message_protocols/udp_protocol.h"
#include "message_protocols/tcp_protocol.h"

namespace atalanta {

/**
 * hybrid_protocol: A message protocol that employs both UDP and TCP
 * transmissions of messages. Normal sends and normal multicasts go via
 * UDP, but relaible_sends and reliable_multicasts go over TCP.
 */
template <size_t MaxSize, class Node>
class hybrid_protocol : public udp_protocol<MaxSize, Node>, 
                        public tcp_protocol<MaxSize, Node> {
    public:
        typedef typename message_protocol<MaxSize>::endpoint endpoint;
        typedef std::shared_ptr<msg_buffer> buffer_ptr;

        /**
         * Constructs the hybrid_protocol feature on the given service and
         * port.
         *
         * @param service The io_service to schedule events on.
         * @param port The port to listen on.
         */
        hybrid_protocol( boost::asio::io_service & service, uint16_t port )
            : udp_protocol<MaxSize, Node>( service, port ),
            tcp_protocol<MaxSize, Node>( service, port ) {
            LOG_FUNCTION_START();
            // nothing
        }

        /**
         * Sends a message to the desired target. Uses UDP to send, so it
         * may not be reliable---however, it is cheap.
         *
         * @param target The desired destination for this message.
         * @param buffer A pointer to the msg_buffer containing the message
         *  to be sent.
         */
        virtual void send_message( const endpoint & target, buffer_ptr buffer ) {
            LOG_FUNCTION_START();
            udp_protocol<MaxSize, Node>::send_message( target, buffer );
        }

        /**
         * Sends a multicast message to the desired targets. Uses UDP to
         * send, so it may not be reliable---however, it is cheap.
         *
         * @param targets The destinations for the message.
         * @param buffer A pointer to the msg_buffer containing the message
         *  to be sent.
         */
        virtual void multicast( const std::vector<endpoint> & targets,
                buffer_ptr buffer ) {
            LOG_FUNCTION_START();
            udp_protocol<MaxSize, Node>::multicast( targets, buffer );
        }

        /**
         * Sends a reliable message to the desired endpoint. This is done
         * by using TCP.
         *
         * @param target The destination for this message.
         * @param buffer A pointer to the msg_buffer that contains the
         *  message to be sent.
         */
        virtual void reliable_send( const endpoint & target, buffer_ptr buffer ) {
            LOG_FUNCTION_START();
            tcp_protocol<MaxSize, Node>::reliable_send( target, buffer );
        }

        /**
         * Sends a reliable multicast to the desired endpoints. This is
         * done by using TCP and the R-Multicast algorithm.
         *
         * @param targets The destinations for this message.
         * @param buffer A pointer to the msg_buffer containing the message
         *  to be sent.
         */
        virtual void reliable_multicast(
                const std::vector<endpoint> & targets, buffer_ptr buffer ) {
            LOG_FUNCTION_START();
            tcp_protocol<MaxSize, Node>::reliable_multicast( targets, buffer );
        }

        /**
         * Message handler for the protocol---only invoked when receiving
         * UDP packets, since TCP packets are handled in the connection
         * subclass.
         *
         * @param error An error if one occurred while attempting to
         *  receive the message.
         * @param bytes_read The number of bytes that were read.
         */
        virtual void handle_message( 
                const boost::system::error_code & error, size_t bytes_read ) {
            LOG_FUNCTION_START();
            udp_protocol<MaxSize, Node>::handle_message( error, bytes_read );
        }
};

}
#endif
