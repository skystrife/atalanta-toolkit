CXX = g++
CXXFLAGS = -I./ -std=c++11 -g -O0 -Wall -Werror
LDFLAGS = -lboost_program_options -lboost_system -lboost_thread -lpthread

SERVER = hb_server
SERVER_OBJS = main.o

EXES = $(SERVER)

all: $(EXES)

main.o: $(wildcard */*.h) $(wildcard */*.cpp) $(wildcard *.cpp)
	$(CXX) $(CXXFLAGS) -c main.cpp -o $@

$(SERVER): $(SERVER_OBJS)
	$(CXX) $(LDFLAGS) $^ -o $@

clean:
	-rm -rf $(EXES) $(SERVER_OBJS)

doc: $(wildcard */*.h) $(wildcard */*.cpp) $(wildcard *.cpp) $(wildcard *.h)
	doxygen atalanta.doxygen
