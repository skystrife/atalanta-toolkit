/**
 * @file failure_detector.h
 * Defines an observer interface for failure detectors.
 */
#ifndef _FAILURE_DETECTOR_H_
#define _FAILURE_DETECTOR_H_

#include <stdint.h>

#include <memory>

#include "message_protocols/message_protocol.h"
#include "failure_detectors/member_entry.h"

namespace atalanta {

/**
 * failure_detector: Defines an interface for failure detectors. All
 * failure detectors should extend this interface and invoke the event
 * functions appropriately.
 */
template <size_t MaxSize>
class failure_detector {
    public:
        typedef typename message_protocol<MaxSize>::endpoint endpoint;
        typedef std::function<void( const endpoint & )> failure_handler;

        /**
         * Adds a handler to the set of handlers to be invoked when a
         * node's failure has been detected.
         *
         * @param handler The handler to be added.
         */
        void add_failure_handler( const failure_handler & handler ) {
            _handlers.push_back( handler );
        }

        /**
         * Invoked when a node's failure has been detected in the system.
         * Calls all registered failure_handlers.
         */
        void on_failure( const endpoint & failed ) {
            for( auto & handler : _handlers )
                handler( failed );
        }

        /**
         * Returns a list of all members known by the current node.
         */
        virtual std::vector<member_entry> all_members() const = 0;

        /**
         * Returns the current node's member_entry.
         */
        virtual const member_entry & self() const = 0;

    private:
        std::vector<failure_handler> _handlers;
};

}
#endif
