/**
 * @file member_entry.h
 * A definition of a membership list entry for membership algorithms.
 */
#ifndef _MEMBER_ENTRY_H_
#define _MEMBER_ENTRY_H_
#include <stdint.h>

#include <sstream>

#include <boost/date_time/posix_time/posix_time.hpp>

#include "containers/circular_list.h"
#include "message_protocols/msg_buffer.h"

namespace atalanta {

/**
 * member_entry: represents a single entry in the membership list.
 */
class member_entry {
    public:
        typedef std::shared_ptr<msg_buffer> buffer_ptr;

        /**
         * Constructs a default member_entry.
         */
        member_entry() : _ip( "127.0.0.1" ), _port( 0 ), 
                _timestamp(
                    boost::posix_time::second_clock::universal_time() ) {
            // nothing
        }

        /**
         * Constructs a new member_entry from the given ip and port.
         *
         * @param p_ip the ip
         * @param p_port the port
         * @param p_timestamp the timestamp
         */
        member_entry( const std::string & p_ip, uint16_t p_port, 
                const boost::posix_time::ptime & p_timestamp ) :
            _ip( p_ip ), _port( p_port ), _timestamp( p_timestamp ) {
            // nothing
        }

        /**
         * Getter for the ip address for this member.
         *
         * @return the member's ip address.
         */
        const std::string & ip() const {
            return _ip;
        }

        /**
         * Setter for the ip address of this member.
         *
         * @param p_ip the ip address to set for this member.
         */
        void ip( const std::string & p_ip ) {
            _ip = p_ip;
        }

        /**
         * Getter for the port for this member.
         *
         * @return the port this member listens on.
         */
        uint16_t port() const {
            return _port;
        }

        /**
         * Setter for the port number of this member.
         *
         * @param p_port Desired port for this member.
         */
        void port( uint16_t p_port ) {
            _port = p_port;
        }

        /**
         * Getter for the timestamp this member joined.
         *
         * @return the timestamp for when the member joined.
         */
        const boost::posix_time::ptime & timestamp() const {
            return _timestamp;
        }

        /**
         * Setter for the timestamp.
         *
         * @param p_timestamp the desired timestamp for this member.
         */
        void timestamp( const boost::posix_time::ptime & p_timestamp ) {
            _timestamp = p_timestamp;
        }

        static member_entry decode_member_entry( buffer_ptr buffer ) {
            LOG_FUNCTION_START();
            std::stringstream ipss;
            // decode ip address (4 bytes)
            for( size_t i = 0; i < 4; ++i ) {
                uint8_t part = 0;
                *buffer >> part;
                // cast required to print integer value rather than a
                // character
                ipss << static_cast<int>( part );
                if( i != 3 )
                    ipss << ".";
            }
            std::string ip = ipss.str();

            // decode port (2 bytes)
            uint16_t port = 0;
            *buffer >> port;

            // decode timestamp (4 bytes)
            int32_t unix_time = 0;
            *buffer >> unix_time;

            return member_entry( 
                    ip, 
                    port, 
                    boost::posix_time::from_time_t( 
                        static_cast<time_t>( unix_time ) 
                    ) 
                );
        }

        static void encode_member_entry( const member_entry & member,
                buffer_ptr buffer ) {
            LOG_FUNCTION_START();
            // denote ip address (4 bytes)
            char dot;
            int part;
            std::istringstream ss( member.ip() );
            for( size_t i = 0; i < 4; ++i ) {
                ss >> part;
                *buffer << static_cast<char>( part );
                if( i != 3 )
                    ss >> dot;
            }

            // denote port (2 bytes)
            uint16_t my_port = member.port();
            *buffer << my_port;

            // denote timestamp (4 bytes)
            boost::posix_time::ptime timestamp( member.timestamp() );
            boost::posix_time::ptime epoch( boost::gregorian::date( 1970, 1, 1 ) );
            int32_t unix_time = (timestamp - epoch).total_seconds();

            *buffer << unix_time;
        }

        /**
         * Determines of two member_entries are the same.
         *
         * @param other The second member_entry to compare against.
         */
        bool operator==( const member_entry & other ) const {
            return ip() == other.ip() && port() == other.port() 
                && timestamp() == other.timestamp();
        }

        /**
         * Determines if two member_entries are different.
         *
         * @param other The second member_entry to compare against.
         */
        bool operator!=( const member_entry & other ) const {
            return !operator==( other );
        }

        /**
         * Gets this member entry as a string.
         */
        std::string to_string() const {
            std::stringstream ss;
            ss << ip() << ":" << port() << ":" << timestamp();
            return ss.str();
        }

    private:
        /**
         * The ip address of this member.
         */
        std::string _ip;

        /**
         * The port this member is listening on.
         */
        uint16_t _port;

        /**
         * The local UTC timestamp this node attempted to join the network
         * at.
         */
        boost::posix_time::ptime _timestamp;
};

}

namespace std {
    template <>
    struct hash<atalanta::member_entry> {
        size_t operator()( const atalanta::member_entry & to_hash ) const {
            std::stringstream ss;
            ss << to_hash.ip() << ":" << to_hash.port() << ":" << to_hash.timestamp();
            return std::hash<std::string>()( ss.str() );
        }
    };
}
#endif
