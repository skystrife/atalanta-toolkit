/**
 * @file all_to_all_heartbeating.h
 * Definition for an all-to-all heartbeating failure detector feature for
 * distributed system nodes.
 */
#ifndef _ALL_TO_ALL_HEARTBEATING_H_
#define _ALL_TO_ALL_HEARTBEATING_H_

#include <stdint.h>

#include <unordered_map>
#include <fstream>

#include <boost/program_options.hpp>

#include "failure_detectors/failure_detector.h"
#include "failure_detectors/member_entry.h"

#include "message_protocols/message_protocol.h"
#include "message_protocols/msg_buffer.h"

using namespace std::placeholders;

namespace atalanta {

/**
 * all_to_all_heartbeating: A failure detector that employs the all-to-all
 * heatbeating algorithm to detect failures. This is sort of a toy example
 * and probably should not be used for distributed systems that involve a
 * large number of nodes for obvious scalability reasons.
 */
template <size_t MaxSize, class Node>
class all_to_all_heartbeating : public failure_detector<MaxSize> {
    public:
        typedef typename message_protocol<MaxSize>::endpoint endpoint;
        typedef std::shared_ptr<msg_buffer> buffer_ptr;
        typedef std::shared_ptr<boost::asio::deadline_timer> timer_ptr;

        /**
         * Constructs the failure detector, installs message handlers, and
         * starts the heartbeating process. If not the first node, it will
         * attempt to join the network by contacting the contact point in
         * members.ini.
         *
         * @param service the io_service to schedule events on.
         * @param port the por to listen on.
         */
        all_to_all_heartbeating( boost::asio::io_service & service, uint16_t port ) :
            _node( static_cast<Node *>(this) ), _service( service ),
            _hb_send_timer( service ) {

            LOG_FUNCTION_START();

            set_up_messages();
            join_network();
            start_heartbeating();
        }

        /**
         * Sends a leave message to the network.
         */
        void send_leave() {
            buffer_ptr buffer = std::make_shared<msg_buffer>();
            buffer->set_msg_id( _leave_msg_id );
            _node->reliable_multicast( get_targets(), buffer );
        }

        std::vector<member_entry> all_members() const {
            LOG_FUNCTION_START();
            std::vector<member_entry> list;
            list.reserve( _member_timers.size() + 1 );
            list.push_back( _self );
            for( auto & p : _member_timers )
                list.push_back( p.first );
            return list;
        }

        const member_entry & self() const {
            return _self;
        }

    private:
        /**
         * Installs callbacks for the failure detector.
         */
        void set_up_messages() {
            LOG_FUNCTION_START();

            _heartbeat_msg_id = _node->add_message( 
                    std::bind( 
                        &all_to_all_heartbeating<MaxSize, Node>::handle_heartbeat,
                        this, _1, _2 )
                );

            _join_msg_id = _node->add_message(
                    std::bind(
                        &all_to_all_heartbeating<MaxSize, Node>::handle_join,
                        this, _1, _2 )
                );

            _fail_msg_id = _node->add_message(
                    std::bind(
                        &all_to_all_heartbeating<MaxSize, Node>::handle_fail_msg,
                        this, _1, _2 )
                );

            _new_mem_msg_id = _node->add_message(
                    std::bind(
                        &all_to_all_heartbeating<MaxSize, Node>::handle_new_member,
                        this, _1, _2 )
                );

            _mem_list_msg_id = _node->add_message(
                    std::bind(
                        &all_to_all_heartbeating<MaxSize, Node>::handle_member_list,
                        this, _1, _2 )
                );

            _leave_msg_id = _node->add_message(
                    std::bind(
                        &all_to_all_heartbeating<MaxSize, Node>::handle_leave,
                        this, _1, _2 )
                );
        }

        /**
         * Attempts to join the network.
         */
        void join_network() {
            LOG_FUNCTION_START();
            boost::program_options::variables_map vm;
            parse_config( vm, "members.ini" );

            boost::posix_time::ptime timestamp( 
                    boost::posix_time::second_clock::universal_time()
                );

            _self = member_entry( vm["self.ip"].as<std::string>(),
                    vm["self.port"].as<uint16_t>(), timestamp );

            std::stringstream selfss;
            selfss << _self.ip() << ":" << _self.port();

            std::stringstream contactss;
            contactss << vm["contact.ip"].as<std::string>() << ":"
                << vm["contact.port"].as<uint16_t>();

            if( selfss.str() != contactss.str() ) {
                LOG( trace ) << _self.port() << ENDLG;
                send_join_message( endpoint( vm["contact.ip"].as<std::string>(), vm["contact.port"].as<uint16_t>() ) );
                LOG( trace ) << _self.port() << ENDLG;
            }
        }

        /**
         * Parses a configuration file to determine the current node's ip
         * and port, as well as the ip and port of a contact node. The
         * contact node is the node to which any join-specific
         * communication will take place.
         *
         * @param vm The variables_map to populate.
         * @param filename The filename to read the configuration data
         *  from.
         */
        void parse_config( boost::program_options::variables_map & vm,
                const std::string & filename ) {
            LOG_FUNCTION_START();
            namespace po = boost::program_options;

            po::options_description opts( "Membership list options" );
            opts.add_options()
                (
                    "self.port",
                    po::value<uint16_t>()->required(),
                    "port for this server"
                )
                (
                    "self.ip",
                    po::value<std::string>()->required(),
                    "ip for this server"
                )
                (
                    "contact.port",
                    po::value<uint16_t>()->required(),
                    "contact port"
                )
                (
                    "contact.ip",
                    po::value<std::string>()->required(),
                    "contact ip"
                );

            std::ifstream file( filename );
            po::store( po::parse_config_file( file, opts ), vm );
            try {
                po::notify( vm );
            } catch( std::exception & ex ) {
                LOG( fatal ) << ex.what() << ENDLG;
                std::terminate();
            }
        }

        /**
         * Sends a join message to the contact node.
         *
         * @param ep The endpoint to send a join message to.
         */
        void send_join_message( const endpoint & ep ) {
            LOG( trace ) << _self.port() << ENDLG;
            buffer_ptr buffer = std::make_shared<msg_buffer>();
            buffer->set_msg_id( _join_msg_id );
            LOG( trace ) << "sending join message with data: " 
                << _self.ip() << ":" << _self.port() << " " 
                << _self.timestamp() << ENDLG;
            member_entry::encode_member_entry( _self, buffer );
            _node->reliable_send( ep, buffer );
        }

        /**
         * Schedules heartbeat events and monitoring.
         */
        void start_heartbeating() {
            LOG_FUNCTION_START();
            schedule_heartbeating();
            schedule_monitoring();
        }

        /**
         * Schedules monitoring for all nodes in the network.
         */
        void schedule_monitoring() {
            for( auto & entry : _member_timers )
                schedule_monitoring( entry.first );
        }

        /**
         * Schedules monitoring for a single node in the network.
         *
         * @param mem The node to schedule monitoring for.
         */
        void schedule_monitoring( const member_entry & mem ) {
            auto it = _member_timers.find( mem );
            if( it != _member_timers.end() ) {
                it->second->cancel();
                it->second->expires_from_now( boost::posix_time::seconds( 5 ) );
                it->second->async_wait( std::bind(
                            &all_to_all_heartbeating<MaxSize, Node>::handle_failure, 
                            this, it->first, _1 )
                        );
            } else {
                LOG( warning ) << "tried to schedule monitoring for "
                    "nonexistent node. ignoring..." << ENDLG;
            }
        }

        /**
         * Schedules heartbeat sending.
         */
        void schedule_heartbeating() {
            _hb_send_timer.cancel();
            _hb_send_timer.expires_from_now( boost::posix_time::seconds( 2 ) );
            _hb_send_timer.async_wait( std::bind(
                        &all_to_all_heartbeating<MaxSize, Node>::send_heartbeat,
                        this, _1 )
                    );
        }

        /**
         * Sends a heartbeat to all other nodes in the network.
         *
         * @param error An error encountered by the timer.
         */
        void send_heartbeat( const boost::system::error_code & error ) {
            LOG_FUNCTION_START();
            if( !error ) {
                buffer_ptr buffer = std::make_shared<msg_buffer>();
                buffer->set_msg_id( _heartbeat_msg_id );
                _node->multicast( get_targets(), buffer );
                schedule_heartbeating();
            } else if( error.value() != boost::system::errc::operation_canceled ) {
                LOG( error ) << error.message() << ENDLG;
            }
        }

        /**
         * Handler for the receipt of a heatbeat.
         *
         * @param buffer The buffer_ptr that contains the data of the
         *  heartbeat message.
         */
        void handle_heartbeat( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            LOG( trace ) << "received hb from " << sender << ENDLG;
            auto ep_it = _endpoint_map.find( sender );
            if( ep_it != _endpoint_map.end() ) {
                schedule_monitoring( ep_it->second );
            } else {
                LOG( warning ) << "received heartbeat from node not in the "
                    "membership list" << ENDLG;
            }
        }

        /**
         * Callback for when a node is detected as having failed.
         *
         * @param failed The member list entry that was detected as having
         *  failed.
         * @param error An error with the timer, if one occurred.
         */
        void handle_failure( const member_entry & failed, 
                const boost::system::error_code & error ) {
            LOG_FUNCTION_START();
            if( !error ) {
                LOG( warning ) << "detected failure of " << failed.ip() 
                    << ":" << failed.port() << ENDLG;
                remove_member( failed );
                buffer_ptr buffer = std::make_shared<msg_buffer>();
                buffer->set_msg_id( _fail_msg_id );
                member_entry::encode_member_entry( failed, buffer );
                _node->reliable_multicast( get_targets(), buffer );
            } else if( error.value() != boost::system::errc::operation_canceled ) {
                LOG( error ) << error.message() << ENDLG;
            }
        }

        /**
         * Callback for when a node receives a message saying that another
         * node has failed in the system. Needed to ensure that if *any*
         * node detects a failure, *all* nodes remove that node. This could
         * probably be improved with a suspicion count or voting...
         *
         * @param buffer The buffer_ptr containing the data of the fail
         *  message.
         */
        void handle_fail_msg( buffer_ptr buffer, const endpoint & sender ) {
            member_entry failed = member_entry::decode_member_entry( buffer );
            remove_member( failed );
        }

        /**
         * Callback for when a join message is received.
         *
         * @param buffer The buffer_ptr containing the data of the join
         *  message.
         */
        void handle_join( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();

            member_entry new_member = member_entry::decode_member_entry( buffer );
            LOG( info ) << "received join message with data: "
                << new_member.ip() << ":" << new_member.port() << " "
                << new_member.timestamp() << ENDLG;

            for( auto & ent : _member_timers ) {
                if( ent.first == new_member ) {
                    LOG( warning ) << "member is already part of the member list, ignoring" << ENDLG;
                    return;
                }
            }

            send_member_list( new_member );

            // modify the message to have the "new member" type
            buffer->set_msg_id( _new_mem_msg_id );
            // broadcast
            _node->reliable_multicast( get_targets(), buffer );
            add_new_member( new_member );
            start_heartbeating();
        }

        /**
         * Callback for when a new_member multicast is received.
         *
         * @param buffer The buffer_ptr containing the data of the
         *  new_member multicast message.
         */
        void handle_new_member( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            member_entry new_member = member_entry::decode_member_entry( buffer );
            add_new_member( new_member );
            start_heartbeating();
        }

        /**
         * Callback for when a membership list is received.
         *
         * @param buffer The buffer_ptr containing the membership list that
         *  was sent from the contact node.
         */
        void handle_member_list( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            if( _member_timers.size() > 0 ) {
                LOG( warning ) << "received a membership list, but one "
                    "already exists. ignoring..." << ENDLG;
                return;
            }

            uint32_t num_nodes = 0;
            *buffer >> num_nodes;

            for( uint32_t i = 0; i < num_nodes; ++i )
                add_new_member( member_entry::decode_member_entry( buffer ) );

            start_heartbeating();
        }

        /**
         * Sends the current membership list to a given node.
         *
         * @param new_member The member to send a membership list to.
         */
        void send_member_list( const member_entry & new_member ) {
            LOG_FUNCTION_START();
            endpoint ep( new_member.ip(), new_member.port() );

            std::vector<member_entry> members;
            for( auto & ent : _member_timers )
                members.push_back( ent.first );
            members.push_back( _self );

            buffer_ptr buffer = std::make_shared<msg_buffer>();
            buffer->set_msg_id( _mem_list_msg_id );
            *buffer << static_cast<uint32_t>( members.size() );
            for( auto & mem_entry : members )
                member_entry::encode_member_entry( mem_entry, buffer );

            _node->reliable_send( ep, buffer ); 
        }

        /**
         * Handles a leave message.
         *
         * @param buffer The buffer_ptr containing the data sent with the
         *  leave message.
         */
        void handle_leave( buffer_ptr buffer, const endpoint & sender ) {
            for( auto & ent : _member_timers ) {
                if( ent.first.ip() == sender.ip() && ent.first.port() == sender.port() ) {
                    _member_timers.erase( ent.first );
                    return;
                }
            }
            LOG( warning ) << "received leave message from node not in the "
                "membership list, ignoring..." << ENDLG;
        }

        
        /**
         * Helper function for translating all keys of the membership list
         * map to a vector of endpoints to send a multicast to.
         */
        std::vector<endpoint> get_targets() const {
            std::vector<endpoint> targets;
            for( auto & ent : _member_timers )
                targets.push_back( endpoint( ent.first.ip(), ent.first.port() ) );
            return targets;
        }

        /**
         * Helper function for adding a new member_entry to the maps.
         *
         * @param new_member The new member entry to add.
         */
        void add_new_member( const member_entry & new_member ) {
            _member_timers.insert( 
                    std::make_pair( 
                        new_member,
                        std::make_shared<boost::asio::deadline_timer>( _service )
                    ) 
                );
            endpoint ep( new_member.ip(), new_member.port() );
            _endpoint_map.insert( std::make_pair( ep, new_member ) );
        }

        /**
         * Helper function for removing a member_entry from the maps.
         *
         * @param failed The member to remove.
         */
        void remove_member( const member_entry & failed ) {
            _member_timers.erase( failed );
            endpoint ep( failed.ip(), failed.port() );
            _endpoint_map.erase( ep );
        }

        /**
         * A pointer to the current object as a node class.
         */
        Node * _node;

        /**
         * The service this feature is scheduling callbacks on. Used for
         * creating new timers.
         */
        boost::asio::io_service & _service;

        /**
         * A map of membership list entries to pointers to deadline timer
         * objects. Each of these timers will fire when its respective
         * member has failed to send a heartbeat in "a while."
         */
        std::unordered_map<member_entry, timer_ptr> _member_timers;

        /**
         * A membership list entry for the current node. Used for sending
         * a complete membership list to new nodes attempting to join the
         * network.
         */
        member_entry _self;

        /**
         * A mapping of endpoints to membership list entries.
         */
        std::unordered_map<endpoint, member_entry> _endpoint_map;

        /**
         * A timer that fires when it is time to send a heartbeat to all
         * other nodes.
         */
        boost::asio::deadline_timer _hb_send_timer;

        uint8_t _heartbeat_msg_id;
        uint8_t _fail_msg_id;
        uint8_t _join_msg_id;
        uint8_t _new_mem_msg_id;
        uint8_t _mem_list_msg_id;
        uint8_t _leave_msg_id;
        
};

}
#endif
