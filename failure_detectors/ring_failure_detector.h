/**
 * @file ring_failure_detector.h
 * Definition for a ring failure detector feature for distributed system
 * nodes. Provides an interface for ring topologies in which *every node
 * knows about every other node*, but there is a logical ring structure.
 */
#ifndef _RING_FAILURE_DETECTOR_H_
#define _RING_FAILURE_DETECTOR_H_

#include "containers/circular_list.h"
#include "failure_detectors/member_entry.h"

namespace atalanta {

template <size_t MaxSize>
class ring_failure_detector : public failure_detector<MaxSize> {
    public:
        typedef circular_list<member_entry> member_list;

        const member_list & member_ring() const {
            return _ring;
        }

        member_list & member_ring() {
            return _ring;
        }

        const member_entry & next_member() const {
            return *(++_ring.begin());
        }

    private:
        member_list _ring;
};

}
#endif
