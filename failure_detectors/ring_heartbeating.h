/**
 * @file ring_heartbeating.h
 * Definition for a ring heartbeating failure detector feature for
 * distributed system nodes.
 */
#ifndef _RING_HEARTBEATING_H_
#define _RING_HEARTBEATING_H_

#include <stdint.h>

#include <fstream>

#include <boost/program_options.hpp>

#include "containers/circular_list.h"

#include "failure_detectors/failure_detector.h"
#include "failure_detectors/member_entry.h"
#include "failure_detectors/ring_failure_detector.h"

#include "message_protocols/msg_buffer.h"

namespace atalanta {

/**
 * ring_heartbeating: A ring based heartbeating failure detector for nodes
 * in a distributed system.
 */
template <size_t MaxSize, class Node>
class ring_heartbeating : public ring_failure_detector<MaxSize> {
    public:
        typedef typename message_protocol<MaxSize>::endpoint endpoint;
        typedef std::shared_ptr<msg_buffer> buffer_ptr;

        /**
         * member_list: a circularly linked list of member_entries.
         */
        typedef circular_list<member_entry> member_list;

        // needed for template disambiguation
        using ring_failure_detector<MaxSize>::member_ring; 

        /**
         * Constructs the failure detector, installs message handlers, and
         * starts heartbeating. If not the first node, it will attempt to
         * join the network by contacting the contact point in members.ini.
         *
         * @param service the io_service to schedule events on.
         * @param port the port to listen on.
         */
        ring_heartbeating( boost::asio::io_service & service, uint16_t port ) :
                _sending_timer( service ), _receive_timer( service ), 
                _node( static_cast<Node *>( this ) ) { 
            LOG_FUNCTION_START();

            set_up_messages();
            join_network();
            start_heartbeating();
        }

        /**
         * Sends a voluntary leave message to the nodes in the network.
         */
        void send_leave() {
            LOG_FUNCTION_START();
            buffer_ptr buffer = std::make_shared<msg_buffer>();
            buffer->set_msg_id( _leave_msg_id );
            std::vector<endpoint> targets;
            targets.reserve( member_ring().size() );
            for( auto mem : member_ring() )
                targets.push_back( endpoint( mem.ip(), mem.port() ) );
            _node->multicast( targets, buffer );
        }

        std::vector<member_entry> all_members() const {
            LOG_FUNCTION_START();
            return std::vector<member_entry>( member_ring().begin(), member_ring().end() );
        }

        const member_entry & self() const {
            LOG_FUNCTION_START();
            return *member_ring().begin();
        }

    private:

        /**
         * Installs the callbacks for the failure detector.
         */
        void set_up_messages() {
            LOG_FUNCTION_START();
            _heartbeat_msg_id = _node->add_message(
                    std::bind( &ring_heartbeating<MaxSize, Node>::handle_heartbeat,
                        this, _1, _2 )
                );

            _join_msg_id = _node->add_message(
                    std::bind( &ring_heartbeating<MaxSize, Node>::handle_join,
                        this, _1, _2 )
                );

            _new_member_msg_id = _node->add_message(
                    std::bind( &ring_heartbeating<MaxSize, Node>::handle_new_member,
                        this, _1, _2 )
                );

            _mem_list_msg_id = _node->add_message(
                    std::bind( &ring_heartbeating<MaxSize, Node>::handle_member_list,
                        this, _1, _2 )
                );

            _leave_msg_id = _node->add_message(
                    std::bind( &ring_heartbeating<MaxSize, Node>::handle_leave,
                        this, _1, _2 )
                );

            _fail_msg_id = _node->add_message(
                    std::bind( &ring_heartbeating<MaxSize, Node>::handle_failure_msg,
                        this, _1, _2 )
                );

            _reconnect_msg_id = _node->add_message(
                    std::bind( &ring_heartbeating<MaxSize, Node>::handle_reconnect,
                        this, _1, _2 )
                );
        }

        /**
         * Attempts to join the network.
         */
        void join_network() {
            LOG_FUNCTION_START();
            boost::program_options::variables_map vm;
            parse_config( vm, "members.ini" );

            boost::posix_time::ptime timestamp(
                    boost::posix_time::second_clock::universal_time()
                );

            member_entry self( vm["self.ip"].as<std::string>(),
                    vm["self.port"].as<int>(), timestamp );

            auto it = member_ring().begin();
            member_ring().insert( it, self );

            std::stringstream selfss;
            selfss << self.ip() << ":" << self.port();

            std::stringstream contactss;
            contactss << vm["contact.ip"].as<std::string>() << ":" << vm["contact.port"].as<int>();

            if( selfss.str() != contactss.str() ) {
                send_join_message( vm["contact.ip"].as<std::string>(), vm["contact.port"].as<int>() );
            }
        }

        /**
         * Parses a config file for the current node's ip and port as well
         * as the ip and port of a contact node.
         *
         * @param vm The variables_map to modify when parsing the config
         *  file.
         * @param filename The name of the file to be parsed.
         */
        void parse_config( boost::program_options::variables_map & vm,
                const std::string & filename ) {
            LOG_FUNCTION_START();
            namespace po = boost::program_options;

            po::options_description opts( "Membership list options" );
            opts.add_options()
                (
                    "self.port",
                    po::value<int>()->required(),
                    "port for this server"
                )
                (
                    "self.ip",
                    po::value<std::string>()->required(),
                    "ip for this server"
                )
                (
                    "contact.port",
                    po::value<int>()->required(),
                    "contact port"
                )
                (
                    "contact.ip",
                    po::value<std::string>()->required(),
                    "contact ip"
                );

            std::ifstream file( filename );
            po::store( po::parse_config_file( file, opts ), vm );
            try {
                po::notify( vm );
            } catch( std::exception & ex ) {
                LOG( fatal ) << ex.what() << ENDLG;
                std::terminate();
            }
        }

        /**
         * Schedules heartbeating and monitoring.
         */
        void start_heartbeating() {
            LOG_FUNCTION_START();
            schedule_heartbeat();
            schedule_monitor();
        }

        /**
         * Schedules a heartbeat.
         */
        void schedule_heartbeat() {
            //LOG_FUNCTION_START();
            _sending_timer.cancel();
            _sending_timer.expires_from_now( boost::posix_time::seconds(2) );
            _sending_timer.async_wait( 
                    std::bind( &ring_heartbeating<MaxSize, Node>::send_heartbeat,
                        this, _1 ) 
                );
        }

        /**
         * Sets a timer that, if triggered, signals the failure of the
         * preceding node in the ring.
         */
        void schedule_monitor() {
            //LOG_FUNCTION_START();
            _receive_timer.cancel();
            _receive_timer.expires_from_now( boost::posix_time::seconds(5) );
            _receive_timer.async_wait(
                    std::bind( &ring_heartbeating<MaxSize, Node>::handle_failure,
                        this, _1 )
                );
        }

        /**
         * Sends a heartbeat to the target node.
         *
         * @param error An error if one occurred.
         */
        void send_heartbeat( const boost::system::error_code & error ) {
            LOG_FUNCTION_START();
            if( !error ) {
                LOG( trace ) << "member list size: " << member_ring().size() << ENDLG;
                const member_entry & target_member = *(++member_ring().begin());

                LOG( trace ) << "sending heartbeat to " << target_member.ip() << ":" << target_member.port() << ENDLG;

                auto target = _node->create_endpoint( target_member.ip(),
                        target_member.port() );

                buffer_ptr buffer = std::make_shared<msg_buffer>();
                buffer->set_msg_id( _heartbeat_msg_id );

                _node->send_message( target, buffer );

                schedule_heartbeat();
            } else {
                LOG( error ) << error.message() << ENDLG;
            }
        }

        /**
         * Handles the receipt of a heartbeat message. If the node was
         * unexpected, a message is sent to that node to reconnect.
         *
         * @param buffer The data that was sent in the heartbeat.
         */
        void handle_heartbeat( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            member_entry expected = *( --member_ring().begin() );
            if( sender.ip() != expected.ip() || sender.port() != expected.port() ) {
                LOG( warning ) << "node " << sender << " was not what was "
                    "expected, telling it to reconnect" << ENDLG;
                send_reconnect_msg( sender );
            }
            // reset timer
            schedule_monitor();
        }

        /**
         * Sends a message to the heartbeating node to reconnect.
         */
        void send_reconnect_msg( const endpoint & target ) {
            buffer_ptr buffer = std::make_shared<msg_buffer>();
            buffer->set_msg_id( _reconnect_msg_id );
            _node->send_message( target, buffer );
        }

        /**
         * Callback for when a failure is detected.
         *
         * @param error An error if one occurred.
         */
        void handle_failure( const boost::system::error_code & error ) {
            LOG_FUNCTION_START();
            if( !error ) {
                if( member_ring().size() > 1 ) {
                    auto it = member_ring().begin();
                    --it;
                    LOG( warning ) << "Detected failure of " << it->ip() << ":" << it->port() << ENDLG;
                    member_entry failed = *it;
                    member_ring().remove( it );
                    std::vector<endpoint> targets;
                    targets.reserve( member_ring().size() );
                    for( auto it = ++member_ring().begin(); it != member_ring().end(); ++it )
                        targets.push_back( endpoint( it->ip(), it->port() ) );
                    auto buffer = std::make_shared<msg_buffer>();
                    buffer->set_msg_id( _fail_msg_id );
                    member_entry::encode_member_entry( failed, buffer );
                    _node->reliable_multicast( targets, buffer );
                }
            } else {
                if( error.value() != boost::system::errc::operation_canceled )
                    LOG( error ) << error.message() << ENDLG;
            }
        }

        /**
         * Message handler for recieving notifications about failed nodes.
         *
         * @param buffer The data sent.
         */
        void handle_failure_msg( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            member_entry failed = member_entry::decode_member_entry( buffer );
            for( auto it = member_ring().begin(); it != member_ring().end(); ++it ) {
                if( it->ip() == failed.ip() && it->port() == failed.port()
                        && it->timestamp() == failed.timestamp() ) {
                    LOG( warning ) << "removing failed node " 
                        << failed.ip() << ":" << failed.port() << ENDLG;
                    member_ring().erase( it );
                    return;
                }
            }
            LOG( error ) << "failed to remove node " << failed.ip() << ":"
                << failed.port() << ENDLG;
        }

        /**
         * Sends a message to the contact node attempting to join the
         * network.
         *
         * @param ip The ip address of the contact node.
         * @param port The port of the contact node.
         */
        void send_join_message( const std::string & ip, int port ) {
            LOG_FUNCTION_START();

            auto target = _node->create_endpoint( ip, port );
            buffer_ptr buffer = std::make_shared<msg_buffer>();

            // denote message type
            buffer->set_msg_id( _join_msg_id );

            // write member entry
            member_entry::encode_member_entry( *member_ring().begin(), buffer );

            _node->reliable_send( target, buffer );
        }

        /**
         * Callback for when a join message is received.
         *
         * @param buffer A pointer to the data in the join message.
         */
        void handle_join( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();

            member_entry new_member = member_entry::decode_member_entry( buffer );
            LOG( info ) << "received join message with data: " 
                << new_member.ip() << ":" << new_member.port() << " " 
                << new_member.timestamp() << ENDLG;

            for( const member_entry & mem : member_ring() ) {
                if( mem.ip() == new_member.ip() 
                        && mem.port() == new_member.port() 
                        && new_member.timestamp() == mem.timestamp() ) {
                    LOG( warning ) << "duplicate join message received, ignoring" << ENDLG;
                    return;
                }
            }

            // modify the message to have the "new member" type
            buffer->set_msg_id( _new_member_msg_id );

            // "reliable multicast" to all targets
            // (on top of udp_protocol, this is somewhat strange)
            std::vector<endpoint> targets;
            targets.reserve( member_ring().size() );
            for( auto it = ++member_ring().begin(); it != member_ring().end(); ++it )
                targets.push_back( endpoint( it->ip(), it->port() ) );

            _node->reliable_multicast( targets, buffer );

            auto it = member_ring().begin();
            LOG( trace ) << "inserting new node after " << it->ip() 
                << ":" << it->port() << ENDLG;
            member_ring().insert( it, new_member ); 
            send_member_list( endpoint( new_member.ip(), new_member.port() ) );
            // restart heartbeating with new topology
            start_heartbeating();
        }

        /**
         * Callback for when a new member message is received.
         *
         * @param buffer A pointer to the data in the new member message.
         */
        void handle_new_member( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            for( auto it = member_ring().begin(); it != member_ring().end(); ++it ) {
                if( it->ip() == sender.ip() && it->port() == sender.port() ) {
                    member_entry new_member = member_entry::decode_member_entry( buffer );
                    auto it2 = it;
                    ++it2;
                    if( it2->ip() == new_member.ip() && it2->port() == new_member.port() ) {
                        LOG( trace ) << "node (" << new_member.ip() << ":"
                            << new_member.port() 
                            << ") already present in the network" << ENDLG;
                        break;
                    }
                    LOG( info ) << "inserting new node after " 
                        << it->ip() << ":" << it->port() << ENDLG;
                    member_ring().insert( it, member_entry::decode_member_entry( buffer ) );
                    // restart heartbeating with new topology
                    start_heartbeating();
                    break;
                }
            }
        }

        /**
         * Sends the current membership list to the desired target node.
         *
         * @param target The endpoint to which a membership list should be
         *  sent.
         */
        void send_member_list( const endpoint & target ) {
            LOG_FUNCTION_START();

            LOG( trace ) << "sending member list to " << target.ip() << ":" << target.port() << ENDLG;
            buffer_ptr buffer = std::make_shared<msg_buffer>();

            // denote message id
            buffer->set_msg_id( _mem_list_msg_id );

            // denote the number of nodes in the ring
            uint32_t num_nodes = static_cast<uint32_t>( member_ring().size() );
            *buffer << num_nodes;

            // write all membership list entries
            for( const member_entry & mem : member_ring() )
                member_entry::encode_member_entry( mem, buffer );

            _node->reliable_send( target, buffer );
        }

        /**
         * Callback for when a membership list was received.
         *
         * @param buffer A pointer to data that comprises the membership
         *  list sent over the network.
         */
        void handle_member_list( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();

            if( member_ring().size() > 1 ) {
                LOG( trace ) << "ignoring duplicate message" << ENDLG;
                return;
            }

            // decode the number of nodes in the ring (4 bytes)
            uint32_t num_nodes = 0;
            *buffer >> num_nodes;

            auto head = member_ring().begin();
            member_ring().erase( head );

            // decode each member list entry and add it to the ring 
            // (each is 10 bytes)
            for( uint32_t i = 0; i < num_nodes; ++i ) {
                auto it = member_ring().begin();
                member_entry mem = member_entry::decode_member_entry( buffer );
                if( member_ring().size() > 0 )
                    LOG( trace ) << "after node: " << it->ip() << ":" << it->port() << ENDLG;
                LOG( trace ) << "rebuilding ring: " << mem.ip() << ":" << mem.port() << ENDLG;
                member_ring().insert_before( it, mem );
            }

            member_ring().set_head( ++member_ring().begin() );
            LOG( trace ) << "head of rebuilt ring: " << member_ring().begin()->ip() << ":" << member_ring().begin()->port() << ENDLG;
        }


        /**
         * Callback for when a voluntary leave message is received.
         *
         * @param buffer A pointer to the data received with the leave
         *  message.
         */
        void handle_leave( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            for( auto it = member_ring().begin(); it != member_ring().end(); ++it ) {
                if( it->ip() == sender.ip() && it->port() == sender.port() ) {
                    member_ring().erase( it );
                    break;
                }
            }
        }

        /**
         * Callback for when the current node is notified that it should
         * rejoin the network with a new id. Used to ensure the crash-stop
         * model holds.
         *
         * @param buffer A pointer to the buffer sent in the reconnect
         *  message.
         */
        void handle_reconnect( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            LOG( warning ) << "reconnecting..." << ENDLG;
            member_ring() = member_list();
            join_network();
            start_heartbeating();
        }

        /**
         * A timer for when the next heartbeat should be sent.
         */
        boost::asio::deadline_timer _sending_timer;

        /**
         * A timer by which the next heartbeat should be received.
         */
        boost::asio::deadline_timer _receive_timer;

        /**
         * A pointer to the current node.
         */
        Node * _node;

        uint8_t _heartbeat_msg_id;
        uint8_t _join_msg_id;
        uint8_t _new_member_msg_id;
        uint8_t _mem_list_msg_id;
        uint8_t _leave_msg_id;
        uint8_t _fail_msg_id;
        uint8_t _reconnect_msg_id;
};

}
#endif
