/**
 * @file atalanta.h
 * Generic include file for atalanta.
 */
#ifndef _ATALANTA_H_
#define _ATALANTA_H_

/**
 * atalanta: Contains all code for the atalanta toolkit.
 */
namespace atalanta {}

// containers
#include "containers/circular_list.h"

// failure detectors
#include "failure_detectors/member_entry.h"
#include "failure_detectors/all_to_all_heartbeating.h"
#include "failure_detectors/ring_heartbeating.h"

// leader election
#include "leader_election/simple_ring_election.h"

// logging framework
#include "log/logger.h"

// message protocols
#include "message_protocols/msg_buffer.h"
#include "message_protocols/message_protocol.h"
#include "message_protocols/udp_protocol.h"
#include "message_protocols/udp_packet_loss_protocol.h"
#include "message_protocols/tcp_protocol.h"
#include "message_protocols/hybrid_protocol.h"

// nodes
#include "nodes/abstract_node.h"
#include "nodes/node.h"

#endif
