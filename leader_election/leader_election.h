/**
 * @file leader_election.h
 * Definition of an abstract class for leader election algorithms to
 * implement.
 */

#ifndef _LEADER_ELECTION_H_
#define _LEADER_ELECTION_H_

#include "failure_detectors/member_entry.h"

namespace atalanta {

class leader_election {
    public:
        virtual const member_entry & leader() const = 0;

        void add_leader_handler( std::function<void()> handler ) {
            _handlers.push_back( handler );
        }

        void on_leader() {
            for( auto & handler : _handlers )
                handler();
        }
    private:
        std::vector<std::function<void()>> _handlers;
};

}


#endif
