/**
 * @file ring_election.h
 * Definition of a simple ring based leader election algorithm for
 * distributed system nodes.
 */
#ifndef _SIMPLE_RING_ELECTION_H_
#define _SIMPLE_RING_ELECTION_H_

#include <functional>
#include <type_traits>
#include <memory>

#include <boost/asio.hpp>

#include "failure_detectors/ring_failure_detector.h"

#include "message_protocols/msg_buffer.h"

#include "leader_election/leader_election.h"

#include "log/logger.h"

namespace atalanta {

/**
 * simple_ring_election: A simplistic ring based leader election algorithm
 * for distributed system nodes. Warning: this algorithm may not guarantee
 * liveness in the event that nodes fail during the election run. For this
 * case, you should pick either the ring_election algorithm or the bully
 * algorithm.
 */
template <size_t MaxSize, class Node>
class simple_ring_election : public leader_election {
    public:
        typedef typename message_protocol<MaxSize>::endpoint endpoint;
        typedef std::shared_ptr<msg_buffer> buffer_ptr;

        simple_ring_election( boost::asio::io_service & service, uint16_t port ) : 
                _node( static_cast<Node *>( this ) ) {
            LOG_FUNCTION_START();
            // require that we have ring topology available to us
            // need to be inside constructor or Node is incomplete type
            static_assert( std::is_base_of<ring_failure_detector<MaxSize>, Node>::value,
                "Ring election requires that the node subclass a failure"
                " detector which provides a ring topology" );

            set_up_messages();
            send_election( _node->self() );
        }

        /**
         * Installs all message handlers for the election protocol.
         */
        void set_up_messages() {
            LOG_FUNCTION_START();

            _election_msg_id = _node->add_message(
                    std::bind( &simple_ring_election<MaxSize, Node>::handle_election,
                        this, _1, _2 )
                );

            _coordinator_msg_id = _node->add_message(
                    std::bind( &simple_ring_election<MaxSize, Node>::handle_coordinator,
                        this, _1, _2 )
                );
        }

        /**
         * Handler invoked when an election message is received.
         *
         * @param buffer The contents of the election message.
         * @param sender The sender of the election message.
         */
        void handle_election( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            member_entry mem = member_entry::decode_member_entry( buffer );
            if( mem == _node->self() ) {
                _leader = _node->self();
                send_coordinator();
            } else {
                if( _node->self().to_string() < mem.to_string() )
                    mem = _node->self();

                send_election( mem );
            }
        }

        /**
         * Sends an election message with the candidate member.
         *
         * @param candidate The current candidate for the election run.
         */
        void send_election( const member_entry & candidate ) {
            LOG_FUNCTION_START();
            LOG( info ) << "Sending election message for candidate: " 
                << candidate.to_string() << ENDLG;

            buffer_ptr buffer = std::make_shared<msg_buffer>();
            buffer->set_msg_id( _election_msg_id );
            member_entry::encode_member_entry( candidate, buffer );

            const member_entry & next = _node->next_member();
            _node->send_message( endpoint( next.ip(), next.port() ), buffer );
        }

        /**
         * Handler invoked when a coordinator message is received.
         *
         * @param buffer The contents of the coordinator message.
         * @param sender The sender of the election message.
         */
        void handle_coordinator( buffer_ptr buffer, const endpoint & sender ) {
            LOG_FUNCTION_START();
            _leader = member_entry::decode_member_entry( buffer );

            LOG( info ) << "Coordinator has been elected: " << _leader.to_string() << ENDLG;
            on_leader();

            if( _leader != _node->self() )
                send_coordinator();
        }

        /**
         * Sends a coordinator message to the next member.
         */
        void send_coordinator() {
            buffer_ptr buffer = std::make_shared<msg_buffer>();
            buffer->set_msg_id( _coordinator_msg_id );
            member_entry::encode_member_entry( _leader, buffer );

            const member_entry & next = _node->next_member();
            _node->send_message( endpoint( next.ip(), next.port() ), buffer );
        }

        /**
         * Returns the current leader that has been elected.
         */
        virtual const member_entry & leader() const {
            return _leader;
        }
    private:
        uint8_t _election_msg_id;
        uint8_t _coordinator_msg_id;
        Node * _node;
        member_entry _leader;
};

}
#endif
